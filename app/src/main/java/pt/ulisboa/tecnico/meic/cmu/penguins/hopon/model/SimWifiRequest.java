
package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

/**
 * Created by Joao on 16/05/2018.
 */

public class SimWifiRequest {

    private String action;
    private String params;
    private String sender;

    public SimWifiRequest(String action, String params, String sender) {
        this.action = action;
        this.params = params;
        this.sender = sender;
    }

    public String getAction() {
        return action;
    }
    public String getParams() {
        return params;
    }
    public String getSender() {
        return sender;
    }
}
