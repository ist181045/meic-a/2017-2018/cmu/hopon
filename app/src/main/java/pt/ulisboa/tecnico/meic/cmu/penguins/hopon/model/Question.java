package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

import java.util.ArrayList;
import java.util.List;

import com.squareup.moshi.Json;

/**
 * Created by Joao on 26/03/2018.
 */

public class Question {

  private int id;
  @Json(name = "text")
  private String title;
  private List<Answer> answers;


  public Question(int id, String title, ArrayList<Answer> answers) {
    this.id = id;
    this.title = title;
    this.answers = answers;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Answer getAnswer(int index) {
    return answers.get(index);
  }

  public void addAnswer(Answer newAnswer) {
    this.answers.add(newAnswer);
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(ArrayList<Answer> answers) {
    this.answers = answers;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void unSelectEverything() {
    for (Answer answer : answers) {
      answer.setSelected(false);
    }
  }
}
