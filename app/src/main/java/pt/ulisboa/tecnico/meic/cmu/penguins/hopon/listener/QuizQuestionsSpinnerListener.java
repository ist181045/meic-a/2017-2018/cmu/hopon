package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.listener;

import java.lang.ref.WeakReference;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Question;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.Spinner;

public class QuizQuestionsSpinnerListener implements AdapterView.OnItemSelectedListener,
    OnTouchListener {

  boolean userSelect = false;
  private Question question;
  private WeakReference<Spinner> spinnerWeakReference;
  private Callback callback;

  public QuizQuestionsSpinnerListener(
      Question question, Spinner spinner, Callback callback) {
    this.question = question;
    this.callback = callback;
    this.spinnerWeakReference = new WeakReference<>(spinner);
  }


  @Override
  public boolean onTouch(View v, MotionEvent event) {
    userSelect = true;
    return false;
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    if (userSelect) {
      callback.OnItemSelected(position, question, spinnerWeakReference.get());
      userSelect = false;
    }
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }

  public interface Callback {

    void OnItemSelected(int position, Question question, Spinner spinner);
  }
}
