package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

/**
 * Created by Joao on 10/05/2018.
 */
public class QuestionAnswerSubmit {

  private int id;
  private int answer;

  public QuestionAnswerSubmit(int id, int answer) {
    this.id = id;
    this.answer = answer;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAnswer() {
    return answer;
  }

  public void setAnswer(int answer) {
    this.answer = answer;
  }
}
