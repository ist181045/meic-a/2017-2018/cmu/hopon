package pt.ulisboa.tecnico.meic.cmu.penguins.hopon;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter.RankingArrayAdapter;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.listener.DrawerItemSelectedListener;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RankingElement;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RankingListActivity extends BaseActivity {

  private RankingArrayAdapter listAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ranking_list);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(new DrawerItemSelectedListener(drawer, this));

    navigationView.getMenu().getItem(1).setChecked(true);

    ListView listView = findViewById(R.id.rankingListView);

    listAdapter = new RankingArrayAdapter(this, R.layout.ranking_list_element);

    HopOnApi.getInstance().getService().getRankings().enqueue(new Callback<List<RankingElement>>() {
      @Override
      public void onResponse(@NonNull Call<List<RankingElement>> call,
          @NonNull Response<List<RankingElement>> response) {
        if (response.isSuccessful() && response.body() != null) {
          buildRankingList(listView, response.body());
        }
      }

      @Override
      public void onFailure(@NonNull Call<List<RankingElement>> call, @NonNull Throwable t) {
        Toast.makeText(RankingListActivity.this, "Unable to connect to server", Toast.LENGTH_LONG).show();
      }
    });

  }

  private void buildRankingList(ListView listView, List<RankingElement> rankingElements) {
    String username = user.getUsername();
    int i;
    if ((i = rankingElements.indexOf(new RankingElement(username, 0, 0))) != -1) {
      View footer = findViewById(R.id.footer);
      ((TextView) footer.findViewById(R.id.ranking_username)).setText(username);
      Date date = new Date(rankingElements.get(i).getTime());
      ((TextView) footer.findViewById(R.id.ranking_points))
          .setText(
                  String.format(Locale.getDefault(), "Score: %d   Time %tM:%tS", rankingElements.get(i).getPoints(), date, date));
      ((TextView) footer.findViewById(R.id.ranking_position))
          .setText(String.format(Locale.getDefault(), "%d", (i + 1)));
    }

    listAdapter.addAll(rankingElements);
    listView.setAdapter(listAdapter);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }
}
