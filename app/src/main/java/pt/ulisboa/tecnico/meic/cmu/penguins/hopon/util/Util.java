package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Util {

  public static void showKeyboard(Context context, View v) {

    InputMethodManager keyboard = (InputMethodManager) context.getSystemService(
        Context.INPUT_METHOD_SERVICE);
    if (keyboard != null) {
      keyboard.showSoftInput(v, 0);
    }
  }

  public static void hideKeyboard(Activity activity) {

    InputMethodManager inputManager = (InputMethodManager)
        activity.getSystemService(Context.INPUT_METHOD_SERVICE);

    if (inputManager != null && activity.getCurrentFocus() != null) {
      inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
          InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }
}
