package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RelayedResponse;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;

import android.util.Base64;
import android.util.Log;

public class Security {

  //TODO: Improve exception handling
  private final static String TAG = "Security";

  public static String encryptRequest(String body) {

    PublicKey pubKey = HopOnApi.getCert().getPublicKey();
    Cipher cipher = null; //or try with "RSA"
    String encoded = "";
    byte[] encrypted = null;
    try {
      cipher = Cipher.getInstance(pubKey.getAlgorithm() + "/ECB/PKCS1Padding");
      cipher.init(Cipher.ENCRYPT_MODE, pubKey);
      encrypted = cipher.doFinal(body.getBytes());
      encoded = Base64.encodeToString(encrypted, Base64.NO_WRAP);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchPaddingException e) {
      e.printStackTrace();
    } catch (BadPaddingException e) {
      e.printStackTrace();
    } catch (IllegalBlockSizeException e) {
      e.printStackTrace();
    } catch (InvalidKeyException e) {
      e.printStackTrace();
    }
    return encoded;

  }

  public static String decryptResponse(String message, byte[] key, byte[] iv) {
    byte[] decrypted = null;
    IvParameterSpec ivSpec = new IvParameterSpec(iv);
    SecretKey secret = new SecretKeySpec(key, "AES");

    Cipher cipher;
    try {
      cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      cipher.init(Cipher.DECRYPT_MODE, secret, ivSpec);
      decrypted = cipher.doFinal(Base64.decode(message, Base64.NO_WRAP));
    } catch (GeneralSecurityException gse) {
      gse.printStackTrace();
    }
    return decrypted == null ? "" : new String(decrypted);

  }

  public static String generateHashWithHmac256(String message, String key) {
    String messageDigest = "";
    try {

      final String hashingAlgorithm = "HmacSHA256";

      byte[] bytes = hmac(hashingAlgorithm, key.getBytes(), message.getBytes());

      messageDigest = Base64.encodeToString(bytes, Base64.NO_WRAP);

      Log.i(TAG, "message digest: " + messageDigest);


    } catch (Exception e) {
      e.printStackTrace();
    }
    return messageDigest;
  }

  private static byte[] hmac(String algorithm, byte[] key, byte[] message) throws
      NoSuchAlgorithmException, InvalidKeyException {
    Mac mac = Mac.getInstance(algorithm);
    mac.init(new SecretKeySpec(key, algorithm));
    return mac.doFinal(message);
  }

}
