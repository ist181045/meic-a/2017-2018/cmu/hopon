package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

import java.util.List;

/**
 * Created by Joao on 10/05/2018.
 */

public class AnswerRequest {

  private int locationId;
  private List<QuestionAnswerSubmit> questions;
  private long time;

  public AnswerRequest() {
  }

  public AnswerRequest(int locationId, List<QuestionAnswerSubmit> questions, long time) {
    this.locationId = locationId;
    this.questions = questions;
    this.time = time;
  }

  public int getLocationId() {
    return locationId;
  }

  public void setLocationId(int locationId) {
    this.locationId = locationId;
  }

  public List<QuestionAnswerSubmit> getQuestions() {
    return questions;
  }

  public void setQuestions(List<QuestionAnswerSubmit> questions) {
    this.questions = questions;
  }

}
