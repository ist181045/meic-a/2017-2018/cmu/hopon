package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.interceptor;


import static pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util.Security.encryptRequest;
import static pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util.Security.generateHashWithHmac256;

import android.util.Base64;
import com.squareup.moshi.Moshi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RelayRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RelayedResponse;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.SimWifiRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util.Security;

public class RelayInterceptor implements Interceptor {

  private static final String TAG = "RelayInterceptor";

  private static String bodyToString(final Request request) {

    try {
      final Request copy = request.newBuilder().build();
      final Buffer buffer = new Buffer();
      copy.body().writeTo(buffer);
      return buffer.readUtf8();
    } catch (final IOException e) {
      return "did not work";
    }
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    if (checkIfRelay() && chain.request().url().encodedPath().equals("/respond")) {
      SimWifiP2pSocket mCliSocket;
      try {
        mCliSocket = new SimWifiP2pSocket(WifiService.getAvailableNativeDevice().getVirtIp(),
            WifiService.getAvailableNativeDevice().getVirtPort());
      } catch (UnknownHostException e) {
        throw new IOException("Unknown Host:" + e.getMessage(), e);
      }

      byte[] key = new byte[16];
      byte[] iv = new byte[16];
      SecureRandom random = new SecureRandom();

      random.nextBytes(key);
      random.nextBytes(iv);
      PrintWriter writer = new PrintWriter(new OutputStreamWriter(mCliSocket.getOutputStream()));
      writer.println(new Moshi.Builder().build()
          .adapter(SimWifiRequest.class)
          .toJson(generateRequest(chain.request(), key, iv)));
      writer.flush();

      String line;
      BufferedReader reader = new BufferedReader(
          new InputStreamReader(mCliSocket.getInputStream()));
      StringBuilder builder = new StringBuilder();

      while ((line = reader.readLine()) != null) {
        builder.append(line);
      }
      mCliSocket.close();
      String decryptedMessage = Security.decryptResponse(builder.toString(), key, iv);

      String body;
      int code = 418; // I'm a little teapot
      try {
        RelayedResponse relayedResponse = new Moshi.Builder().build()
            .adapter(RelayedResponse.class).fromJson(decryptedMessage);
        body = relayedResponse.getContent();
        code = relayedResponse.getCode();
      } catch (IOException e) {
        body = builder.toString();
      }

      ResponseBody responseBody = ResponseBody.create(MediaType.parse("application/json"), body);
      return new Response.Builder()
          .code(code)
          .protocol(Protocol.HTTP_1_1)
          .message(code == 418 ? "I'm a little teapot" : String.valueOf(code))
          .request(chain.request())
          .body(responseBody)
          .build();
    } else {
      return chain.proceed(chain.request());
    }
  }

  private boolean checkIfRelay() {
    if (WifiService.getDeviceName() != null) {
      return WifiService.getDeviceName().toLowerCase().startsWith("f")
          && WifiService.getCurrentBeacon() == 0;
    }
    return false;
  }

  private SimWifiRequest generateRequest(Request request, byte[] key, byte[] iv) {
    RelayRequest relayRequest = new RelayRequest();
    String bodyString = bodyToString(request);
    String sessionId = request.header("Authz");
    String keyString = Base64.encodeToString(key, Base64.NO_WRAP);
    String ivString = Base64.encodeToString(iv, Base64.NO_WRAP);

    relayRequest.setHmac(
        generateHashWithHmac256(sessionId + keyString + ivString + bodyString, sessionId));
    relayRequest.setKey(keyString);
    relayRequest.setIv(ivString);
    relayRequest.setContent(bodyString);
    relayRequest.setToken(sessionId);
    return new SimWifiRequest("relay", encryptRequest(new Moshi.Builder().build()
        .adapter(RelayRequest.class)
        .toJson(relayRequest)), WifiService.getDeviceName());
  }
}
