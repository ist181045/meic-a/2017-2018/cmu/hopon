package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.IBinder;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.service.SimWifiP2pService;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.LandmarkListActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.QuizActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.R;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.RankingListActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Landmark;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.SimWifiRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.receiver.SimWifiP2pReceiver;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.runnable.SocketReceiveRunnable;

/**
 * Created by Joao on 16/05/2018.
 */

public class WifiService extends Service
    implements SimWifiP2pManager.PeerListListener, SimWifiP2pManager.GroupInfoListener {

  private final static String TAG = "WifiService";
  private final static int landmarkNotificationID = 69;
  private static int lastBeacon;
  private static int currentBeacon;
  private static SimWifiP2pDevice self;
  private static SimWifiP2pDeviceList neighbors = null;
  private static SimWifiP2pDeviceList groupMembers = null;
  private static int notificationId = 70;
  private SimWifiP2pReceiver receiver;
  private SimWifiP2pManager manager = null;
  private SimWifiP2pManager.Channel channel = null;
  private Messenger service = null;

  private ServiceConnection connection = new ServiceConnection() {
    @Override
    public void onServiceConnected(ComponentName className, IBinder service) {
      WifiService.this.service = new Messenger(service);
      manager = new SimWifiP2pManager(WifiService.this.service);
      channel = manager.initialize(getApplicationContext(), getMainLooper(), null);
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
      manager = null;
      channel = null;
      service = null;
    }
  };
  private Thread thread;

  public static List<SimWifiP2pDevice> getDevices() {
    List<SimWifiP2pDevice> devices = new ArrayList<>();
    for (SimWifiP2pDevice device : neighbors.getDeviceList()) {
      if (groupMembers.getDeviceList().contains(device)) {
        devices.add(device);
      }
    }
    return devices;
  }

  public static String getDeviceName() {
    if (self == null) {
      return null;
    }
    return self.deviceName;
  }

  public static SimWifiP2pDevice getAvailableNativeDevice() {
    for (SimWifiP2pDevice device : getDevices()) {
      if (!device.deviceName.toUpperCase().startsWith("F")) {
        return device;
      }
    }
    //TODO: Lulz exception
    return null;
  }

  public static int getLastBeacon() {
    return lastBeacon;
  }

  public static int getCurrentBeacon() {
    return currentBeacon;
  }

  public static int getVirtualPort() {
    return self.getVirtPort();
  }

  @Override
  public void onCreate() {
    super.onCreate();

    Intent serviceIntent = new Intent(this, SimWifiP2pService.class);
    bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);

    IntentFilter filter = new IntentFilter();
    filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION);
    filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION);

    receiver = new SimWifiP2pReceiver(this);
    registerReceiver(receiver, filter);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    unbindService(connection);
    unregisterReceiver(receiver);
    if (thread != null) {
      thread.interrupt();
    }
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onPeersAvailable(SimWifiP2pDeviceList simWifiP2pDeviceList) {
    neighbors = simWifiP2pDeviceList;
    for (SimWifiP2pDevice device : simWifiP2pDeviceList.getDeviceList()) {
      if (device.deviceName.startsWith("M")) {
        Integer beacon = Integer.valueOf(device.deviceName.substring(1));
        if (currentBeacon != beacon) {
          lastBeacon = currentBeacon;
          currentBeacon = beacon;
          displayArrivedToLocation(currentBeacon);
        }
        return;
      }
    }
    lastBeacon = currentBeacon;
    currentBeacon = 0;
  }

  private void displayArrivedToLocation(int currentBeacon) {
    Landmark landmark = LandmarkListActivity.getLandmarks().get(currentBeacon);
    if (landmark != null) {

      Intent intent = new Intent(this, QuizActivity.class);
      intent.putExtra("landmark", landmark.getId());
      Builder noti = new Builder(this)
          .setContentTitle(landmark.getTitle())
          .setContentText("You've arrived " + landmark.getTitle())
          .setSmallIcon(R.mipmap.ic_launcher)
          .setAutoCancel(true)
          .setContentIntent(
              PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

      Notification notification;
      if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
        notification = noti.build();
      } else {
        notification = noti.getNotification();
      }
      NotificationManager manager = (NotificationManager) getSystemService(
          Context.NOTIFICATION_SERVICE);
      if (manager != null) {
        try {
          manager.cancel(landmarkNotificationID);

        } catch (Exception ignored) {
          Log.e(TAG, "Notification error");
        }
        manager.notify(landmarkNotificationID, notification);
      } else {
        Log.e(TAG, "Notification error");
      }

    }
  }

  public void updatePeers() {
    if (manager != null) {
      manager.requestPeers(channel, this);
    }
  }

  public void updateNetwork() {
    if (manager != null) {
      manager.requestGroupInfo(channel, this);
    }
  }

  @Override
  public void onGroupInfoAvailable(SimWifiP2pDeviceList simWifiP2pDeviceList,
      SimWifiP2pInfo simWifiP2pInfo) {
    groupMembers = simWifiP2pDeviceList;
    String deviceName = simWifiP2pInfo.getDeviceName();
    self = simWifiP2pDeviceList.getByName(deviceName);

    if (thread == null) {
      thread = new Thread(new SocketReceiveRunnable(this));
      thread.start();
    }
  }

  public void displayNotification(SimWifiRequest request) {
    Intent intent = new Intent(this, RankingListActivity.class);
    Notification.Builder noti = new Notification.Builder(this)
        .setContentTitle("New notification from " + request.getSender())
        .setContentText(request.getParams())
        .setContentIntent(
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
        .setSmallIcon(R.mipmap.ic_launcher);
    Notification notification;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
      notification = noti.build();
    } else {
      notification = noti.getNotification();
    }
    NotificationManager manager = (NotificationManager) getSystemService(
        Context.NOTIFICATION_SERVICE);
    if (manager != null) {
      manager.notify(notificationId++, notification);
    } else {
      Log.e(TAG, "Notification error");
    }
  }

}
