package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class RelayedResponse {
  private int code;
  private String content;

  public RelayedResponse(int code, String content) {
    this.code = code;
    this.content = content;
  }

  public int getCode() {
    return code;
  }

  public String getContent() {
    return content;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
