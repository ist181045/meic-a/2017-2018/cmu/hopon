package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class RankingElement {

  private String username;
  private int points;
  private long time;

  public RankingElement(String username, int points, long time) {
    this.username = username;
    this.points = points;
    this.time = time;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RankingElement that = (RankingElement) o;
    return username != null && username.equals(that.username);
  }
}
