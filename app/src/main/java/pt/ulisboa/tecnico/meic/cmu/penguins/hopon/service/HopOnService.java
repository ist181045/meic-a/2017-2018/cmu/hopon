package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service;

import java.util.List;

import okhttp3.RequestBody;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.AnswerRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.AuthRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Landmark;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Question;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RankingElement;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HopOnService {

  @POST("/register")
  Call<User> registerUser(@Body AuthRequest authRequest);

  @POST("/login")
  Call<User> login(@Body AuthRequest authRequest);

  @GET("/leaderboards")
  Call<List<RankingElement>> getRankings();

  @GET("/locations")
  Call<List<Landmark>> getLandmarks();

  @POST("/respond")
  Call<String> sendResponses(@Body AnswerRequest answers);

  @Headers("Relayed: true")
  @POST("/respond")
  Call<String> sendRelayResponses(@Body RequestBody request);

  @GET("/quiz")
  Call<List<Question>> getQuiz(@Query("locationId") int locationId);

  @POST("/logout")
  Call<Void> logout();

}
