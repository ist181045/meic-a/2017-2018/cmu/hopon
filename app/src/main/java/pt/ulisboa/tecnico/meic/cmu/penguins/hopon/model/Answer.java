package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class Answer {

  private int id;
  private String text;

  private boolean selected;

  public Answer(int id, String text) {
    this.id = id;
    this.text = text;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String toString() {
    return text;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }
}
