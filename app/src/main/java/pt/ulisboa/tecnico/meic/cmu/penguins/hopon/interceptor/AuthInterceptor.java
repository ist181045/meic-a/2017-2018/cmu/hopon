package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.interceptor;

import java.io.IOException;

import android.support.annotation.NonNull;
import android.util.Log;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

  private static final String TAG = "AuthInterceptor";
  private static final String AUTH_HEADER_NAME = "Authz";
  private String token;

  public AuthInterceptor(String token) {
    this.token = token;
  }

  @Override
  public Response intercept(@NonNull Chain chain) throws IOException {
    Log.d(TAG, token);
    Request request = chain.request();
    Request newRequest = request.newBuilder().addHeader(AUTH_HEADER_NAME, token).build();
    return chain.proceed(newRequest);
  }
}
