package pt.ulisboa.tecnico.meic.cmu.penguins.hopon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter.SpinnerArrayAdapterWithHint;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.listener.QuizQuestionsSpinnerListener;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Answer;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.AnswerRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Landmark;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Question;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.QuestionAnswerSubmit;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.runnable.SendWifiRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material;

public class QuizActivity extends AppCompatActivity
    implements QuizQuestionsSpinnerListener.Callback {

  private Landmark landmark;
  private static final String TAG = "QuizActivity";
  private boolean hasBackgroundImage = false;

  @SuppressLint("PrivateResource")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_quiz);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    int landmarkId;
    Intent intent = getIntent();
    if (savedInstanceState != null &&
        (landmarkId = savedInstanceState.getInt("landmark", -1)) != -1 &&
        (landmark = LandmarkListActivity.landmarks.get(landmarkId)) != null) {
      Log.d(TAG, "Got landmark id" + landmarkId);
    } else if (intent != null &&
        (landmarkId = intent.getIntExtra("landmark", -1)) != -1 &&
        (landmark = LandmarkListActivity.landmarks.get(landmarkId)) != null) {
      Log.d(TAG, "Got landmark id" + landmarkId);
    } else {
      Log.e(TAG, "The landmark wasn't passed with intent or bundled\nGoing back...");
      onBackPressed();
    }

    /*
      Toolbar related calls
     */
    processBackgroundImage();
    toolbar.setNavigationIcon(abc_ic_ab_back_material);
    toolbar.setTitle(landmark.getTitle());
    toolbar.setNavigationOnClickListener(view -> onBackPressed());


    /*
      Process landmark questions
     */
    ViewGroup content = findViewById(R.id.nestedScrollView);

    if (landmark.isAnswered()) {
      String message;
      if(landmark.getCorrect() == -1) {
        message = "You already answered this quiz";
        displayResults(message, content.findViewById(R.id.linear_layout), false);
      }
      else {
        message = String.format(Locale.getDefault(), "You got %d out of %d questions right!",
            landmark.getCorrect(), landmark.getTotal());
        displayResults(message, content.findViewById(R.id.linear_layout), true);
      }
    } else if (landmark.getQuestions().isEmpty()) {
      View downloadView = View
          .inflate(this, R.layout.download_button, content.findViewById(R.id.linear_layout));
      Button downloadButton = downloadView.findViewById(R.id.download_button);
      downloadButton.setOnClickListener(v -> {

        if (WifiService.getCurrentBeacon() != landmark.getId()) {
          String message = "You are not in this location yet.";
          displayResults(message, content.findViewById(R.id.linear_layout), false);
        } else {
          HopOnApi.getInstance().getService().getQuiz(landmark.getId()).enqueue(
              new Callback<List<Question>>() {
                @Override
                public void onResponse(@NonNull Call<List<Question>> call,
                    @NonNull Response<List<Question>> response) {
                  if (response.isSuccessful()) {
                    List<Question> questionList = response.body();
                    if (questionList != null && !questionList.isEmpty()) {
                      landmark.setQuestions(questionList);
                      downloadView.findViewById(R.id.download_layout).setVisibility(View.GONE);
                      populateQuestions(content.findViewById(R.id.linear_layout));
                      return;
                    }
                  }
                  try {
                    ResponseBody rb = response.errorBody();
                    assert rb != null;
                    Toast.makeText(QuizActivity.this, "Error getting questions: " + rb.string(),
                        Toast.LENGTH_LONG).show();
                  } catch (IOException e) {
                    Toast.makeText(QuizActivity.this, "Unknown error occurred", Toast.LENGTH_LONG)
                        .show();
                  }
                }

                @Override
                public void onFailure(@NonNull Call<List<Question>> call, @NonNull Throwable t) {
                  if (t.getMessage() == null || t.getMessage().isEmpty()) {
                    Toast.makeText(QuizActivity.this, "Unknown error occurred", Toast.LENGTH_LONG)
                        .show();
                  } else {
                    Toast.makeText(QuizActivity.this, "An error occurred: " + t.getMessage(),
                        Toast.LENGTH_LONG).show();
                  }
                }
              });
        }
      });
    } else {
      populateQuestions(content.findViewById(R.id.linear_layout));
    }

  }


  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    super.onSaveInstanceState(savedInstanceState);
    savedInstanceState.putInt("landmark", landmark.getId());

  }

  private void populateQuestions(ViewGroup parent) {
    for (Question question : landmark.getQuestions()) {
      View questionItemView = View.inflate(this, R.layout.question_item, null);
      SpinnerArrayAdapterWithHint spinnerArrayAdapterWithHint = new
          SpinnerArrayAdapterWithHint(this, android.R.layout.simple_spinner_dropdown_item);

      spinnerArrayAdapterWithHint.add(new Answer(-1, "Choose One..."));
      //spinnerArrayAdapterWithHint.addAll(question.getAnswers());

      int selected = 0;
      List<Answer> answers = question.getAnswers();
      for (int i = 0; i < answers.size(); i++) {
        spinnerArrayAdapterWithHint.add(answers.get(i));
        if (answers.get(i).isSelected()) {
          selected = i + 1;
        }
      }

      ((TextView) questionItemView.findViewById(R.id.question_title))
          .setText(question.getTitle());
      Spinner answerSpinner = questionItemView.findViewById(R.id.spinner);
      answerSpinner.setAdapter(spinnerArrayAdapterWithHint);
      QuizQuestionsSpinnerListener listener = new QuizQuestionsSpinnerListener(question,
          answerSpinner, this);

      int finalSelected = selected;
      answerSpinner.post(() -> {
        answerSpinner.setOnItemSelectedListener(listener);
        answerSpinner.setOnTouchListener(listener);
        answerSpinner.setSelection(finalSelected, true);
      });

      parent.addView(questionItemView,
          new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    View submitButtonView = View.inflate(this, R.layout.quiz_submit_button, null);
    submitButtonView.findViewById(R.id.quiz_submit_button).setOnClickListener(v -> {

      if (!((WifiService.getCurrentBeacon() == 0 && WifiService.getLastBeacon() == landmark.getId())
          || WifiService.getCurrentBeacon() == landmark.getId())) {
        String message = "You can no longer submit this quiz.";
        displayResults(message, parent, false);
        return;
      }

      List<QuestionAnswerSubmit> answers = new ArrayList<>();

      for (Question question : landmark.getQuestions()) {
        for (Answer answer : question.getAnswers()) {
          if (answer.isSelected()) {
            answers.add(new QuestionAnswerSubmit(question.getId(), answer.getId()));
            break;
          }
        }
      }

      if (answers.size() != landmark.getQuestions().size()) {
        Toast.makeText(this, "You have to answer all the questions", Toast.LENGTH_SHORT).show();
        return;
      }

      landmark.setEndTime(System.currentTimeMillis());

      HopOnApi.getInstance().getService()
          .sendResponses(new AnswerRequest(landmark.getId(), answers,
              landmark.getEndTime() - landmark.getStartTime()))
          .enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
              String message;
              boolean show = false;
              if (response.code() == 200) {
                Integer correct = Integer.valueOf(response.body());
                Date date = new Date(landmark.getEndTime() - landmark.getStartTime());
                message = String
                    .format(Locale.getDefault(),
                        "You got %d out of %d questions right in %tM minutes and %tS seconds!",
                        correct,
                        landmark.getQuestions().size(), date, date);

                landmark.setAnswered(true);
                landmark.setCorrect(correct);
                landmark.setTotal(landmark.getQuestions().size());
                landmark.setQuestions(null);
                show = true;
              } else if (response.code() == 400) { //quiz already answered or invalid location id
                message = "This quiz has already been answered.";
                landmark.setAnswered(true);
                landmark.setCorrect(-1);
                //TODO: need to change stuff on the server
              } else if (response.code() == 403) { //invalid session id
                message ="Invalid session id";
                //TODO: probably should log the user out
                //Toast.makeText(QuizActivity.this, "Invalid session id, logging out...", Toast.LENGTH_LONG).show();
              } else if(response.code() == 418) {
                if(response.body() != null) {
                  message = response.body();
                }
                else {
                  message = "Unable to connect to server.";
                }
              } else {
                message = "An unkown error occured.";
              }
              displayResults(message, parent, show);
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
              Toast.makeText(QuizActivity.this, "Unable to connect to server", Toast.LENGTH_LONG).show();
            }
          });
    });
    parent.addView(submitButtonView);
    landmark.setStartTime(System.currentTimeMillis());
  }

  private void displayResults(String message, ViewGroup group, boolean showShareButton) {
    View display = View.inflate(this, R.layout.quiz_results_display, null);

    ((TextView) display.findViewById(R.id.textView3)).setText(message);
    group.removeAllViews();
    group.addView(display);

    if (showShareButton) {
      View share = View.inflate(this, R.layout.quiz_share_button, null);
      View button = share.findViewById(R.id.share_button);
      button.setOnClickListener(v -> {
        String shareMessage;
        Date date = new Date(landmark.getEndTime() - landmark.getStartTime());
        try {
          User user = User.getInstance();
          shareMessage = String
              .format(Locale.getDefault(),
                  "The user %s scored %d out of %d on the %s quiz in %tM minutes and %tS seconds",
                  user.getUsername(), landmark.getCorrect(), landmark.getTotal(),
                  landmark.getTitle(), date, date);
        } catch (Exception e) {
          //cancer code
          shareMessage = String
              .format(Locale.getDefault(),
                  "Some user scored %d out of %d on the %s quiz in %tM minutes and %tS seconds",
                  landmark.getCorrect(), landmark.getTotal(), landmark.getTitle(), date, date);
        }
        List<SimWifiP2pDevice> devicesList = new ArrayList<>(
            WifiService.getDevices());
        List<SimWifiP2pDevice> deviceList = new ArrayList<>();

        for (SimWifiP2pDevice device : devicesList) {
          if (!device.deviceName.startsWith("M")) {
            deviceList.add(device);
          }
        }

        String[] devices = new String[deviceList.size()];
        for (int i = 0; i < deviceList.size(); i++) {
          devices[i] = deviceList.get(i).deviceName;
        }
        List<SimWifiP2pDevice> devicesToShare = new ArrayList<>();
        String finalShareMessage = shareMessage;
        new AlertDialog.Builder(this)
            .setTitle("Choose the device")
            .setMultiChoiceItems(devices, null, (dialog, which, isChecked) -> {
              SimWifiP2pDevice device = deviceList.get(which);
              if (isChecked) {
                devicesToShare.add(device);
              } else {
                devicesToShare.remove(device);
              }
            })
            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
              shareResult(devicesToShare, finalShareMessage);
            })
            .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
              devicesToShare.clear();
            }).create().show();

      });
      group.addView(share);
    }


  }

  private void shareResult(List<SimWifiP2pDevice> devices, String message) {
    for (SimWifiP2pDevice device : devices) {
      new SendWifiRequest(device.getVirtIp(), device.getVirtPort(), message)
          .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
  }

  @Override
  protected void onStart() {
    super.onStart();
    processBackgroundImage();
  }


  /**
   * Duplicating the call to this method is a workaround for background image not being added to
   * toolbar sometimes TODO: Fix this one day probably by creating a custom view that implements
   * Target {@link Target}
   */
  private void processBackgroundImage() {
    if (!hasBackgroundImage) {
      CollapsingToolbarLayout finalToolbar = findViewById(R.id.toolbar_layout);
      finalToolbar.post(() -> Picasso.get().load(
          landmark.getImageUrl())
          .resize(finalToolbar.getWidth(), finalToolbar.getHeight())
          .centerCrop()
          .into(new Target() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
              if (!hasBackgroundImage) {
                if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
                  finalToolbar.setBackground(new BitmapDrawable(getResources(), bitmap));
                } else {
                  finalToolbar.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
                }
                hasBackgroundImage = true;
                Log.d(TAG, "Add background image to toolbar");
              } else {
                Log.d(TAG, "Already has background image");
              }

            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
              Log.e(TAG, "Bitmap loading failed.");
              Log.e(TAG, e.getMessage());
            }


            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
              Log.d(TAG, "On Prepare load");
            }
          }));
    }
  }

  @Override
  public void OnItemSelected(int position, Question question, Spinner spinner) {
    question.unSelectEverything();
    ((Answer) spinner.getAdapter().getItem(position)).setSelected(true);
  }
}
