package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.v4.content.ContextCompat;
import com.squareup.picasso.Picasso;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.QuizActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.R;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter.LandmarkListAdapter.LandmarkHolder;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Landmark;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public class LandmarkListAdapter extends ListAdapter<Landmark, LandmarkHolder> {

  private static final DiffUtil.ItemCallback<Landmark> LANDMARK_CALLBACK =
      new DiffUtil.ItemCallback<Landmark>() {

        @Override
        public boolean areItemsTheSame(Landmark oldItem, Landmark newItem) {
          return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(Landmark oldItem, Landmark newItem) {
          return oldItem.getTitle().equals(newItem.getTitle())
              && oldItem.isAnswered() == newItem.isAnswered();
        }
      };

  public LandmarkListAdapter() {
    super(LANDMARK_CALLBACK);
  }

  @NonNull
  @Override
  public LandmarkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
    return new LandmarkHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull LandmarkHolder holder, int position) {
    holder.bindTo(getItem(position));
  }

  @Override
  public int getItemViewType(int position) {
    return R.layout.landmark_list_element;
  }

  class LandmarkHolder extends RecyclerView.ViewHolder {

    private final TextView title;
    private final TextView description;
    private final Button openButton;
    private final CircleImageView image;

    LandmarkHolder(View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.ranking_username);
      description = itemView.findViewById(R.id.ranking_points);
      image = itemView.findViewById(R.id.ranking_position);
      openButton = itemView.findViewById(R.id.open_button);
    }

    void bindTo(Landmark landmark) {
      title.setText(landmark.getTitle());
      description.setText(landmark.getDescription());
      Picasso.get().load(landmark.getImageUrl())
          .placeholder(R.drawable.placeholder)
          .into(image);
      if(landmark.isAnswered()) {
          image.setBorderColor(ContextCompat.getColor(image.getContext(), android.R.color.holo_green_dark));
          image.setBorderWidth(5);
      }
      openButton.setOnClickListener(v -> openQuizActivity(landmark, v));
      itemView.setOnClickListener(v -> openQuizActivity(landmark, v));
    }

    private void openQuizActivity(Landmark landmark, View v) {
      int currentBeacon = WifiService.getCurrentBeacon();
      int lastBeacon = WifiService.getLastBeacon();
      if ((currentBeacon == 0 && lastBeacon == landmark.getId()) || currentBeacon == landmark.getId() || landmark.isAnswered()) {
        itemView.getContext()
            .startActivity(new Intent(itemView.getContext(), QuizActivity.class)
                .putExtra("landmark", landmark.getId()));
      }
      else {
        new AlertDialog.Builder(v.getContext())
            .setTitle("Oooopsie")
            .setMessage("You're not near " + landmark.getTitle())
            .setPositiveButton(android.R.string.ok, (dialog, which) -> {})
            .create().show();
      }
    }
  }
}
