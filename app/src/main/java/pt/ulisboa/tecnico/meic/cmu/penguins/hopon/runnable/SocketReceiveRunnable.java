package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.runnable;

import android.util.Log;
import com.squareup.moshi.Moshi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketServer;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.SimWifiRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;
import retrofit2.Response;

/**
 * Created by Joao on 16/05/2018.
 */

public class SocketReceiveRunnable implements Runnable {

  private static final String TAG = "SocketReceiveRunner";

  private WeakReference<WifiService> service;

  public SocketReceiveRunnable(WifiService service) {
    this.service = new WeakReference<>(service);
  }

  @Override
  public void run() {
    SimWifiP2pSocketServer socketServer = null;
    try {
      socketServer = new SimWifiP2pSocketServer(WifiService.getVirtualPort());

      while (!Thread.currentThread().isInterrupted()) {
        Log.d(getClass().getSimpleName(), "Estou à espera...");
        SimWifiP2pSocket socket = socketServer.accept();
        Log.d(TAG, "Received a message through \"Wifi Direct\"");

        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        //StringBuilder builder = new StringBuilder();

        line = reader.readLine();
        /*while (line!= null) {
          builder.append(line);
          line = reader.readLine();
        }*/
//        builder.append(reader.readLine());
//        while (reader.ready()) {
//          builder.append(reader.readLine());
//        }

        SimWifiRequest request = new Moshi.Builder().build().adapter(SimWifiRequest.class)
            .fromJson(line);

        if (request == null || request.getAction() == null || request.getParams() == null) {
          socket.close();
          continue;
        }

        switch (request.getAction()) {
          case "relay":
            relayRequest(request, socket);
            socket.close();
            break;
          case "share":
            service.get().displayNotification(request);
          default:
            socket.close();
            break;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (socketServer != null) {
          socketServer.close();
        }
      } catch (IOException ignored) {
      }
    }
  }

  private void relayRequest(SimWifiRequest request, SimWifiP2pSocket socket) {
    try {
      RequestBody body = RequestBody.create(MediaType.parse("text/plain"), request.getParams());
      Response<String> response = HopOnApi.getInstance().getService()
          .sendRelayResponses(body)
          .execute();
      OutputStream out = socket.getOutputStream();
      if (response.isSuccessful() && response.body() != null) {
        out.write(response.body().getBytes());
      } else {
        String message = "Unable to connect to server.\n";
        out.write(message.getBytes());
      }
      out.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}