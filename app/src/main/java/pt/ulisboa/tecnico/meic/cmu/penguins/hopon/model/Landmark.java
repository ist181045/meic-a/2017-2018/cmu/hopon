package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

import java.util.ArrayList;
import java.util.List;

import com.squareup.moshi.Json;

public class Landmark {

  private int id;
  private Coordinates coordinates;
  @Json(name = "name")
  private String title;
  private String description;
  @Json(name = "imagePath")
  private String imageUrl;
  private List<Question> questions = new ArrayList<>();
  private boolean answered = false;
  private int correct;
  private int total;

  private long startTime;
  private long endTime;

  @SuppressWarnings("unused")
  private Landmark() {
  }

  public Landmark(int id, Coordinates coordinates, String title, String description,
      String imageUrl) {
    this.coordinates = coordinates;
    this.title = title;
    this.description = description;
    this.imageUrl = imageUrl;
    this.id = id;
  }

  public Landmark(int id, Coordinates coordinates, String title, String description,
      String imageUrl,
      List<Question> questions) {
    this.coordinates = coordinates;
    this.title = title;
    this.description = description;
    this.imageUrl = imageUrl;
    this.questions = questions;
    this.id = id;
  }

  public Coordinates getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(Coordinates coordinates) {
    this.coordinates = coordinates;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }

  public boolean isAnswered() {
    return answered;
  }

  public void setAnswered(boolean answered) {
    this.answered = answered;
  }

  public int getCorrect() {
    return correct;
  }

  public void setCorrect(int correct) {
    this.correct = correct;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public long getStartTime() {
    return startTime;
  }

  public void setStartTime(long startTime) {
    this.startTime = startTime;
  }

  public long getEndTime() {
    return endTime;
  }

  public void setEndTime(long endTime) {
    this.endTime = endTime;
  }
}
